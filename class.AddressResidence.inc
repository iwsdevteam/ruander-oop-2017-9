<?php

/* 
 * Állandó lakcím
 */

class AddressResidence extends Address {
    //redeclare
    public $country_name = "Neverland";
    public $valami;
    
    //method override
    public function display(){
        $output = '<div class="residence alert alert-success">';
        $output .= parent::display();
        $output .= '</div>';
        return $output;
    }
    
    protected function _init(){
        $this->_setAddressTypeId(parent::ADDRESS_TYPE_RESIDENCE);
        return;
    }
}