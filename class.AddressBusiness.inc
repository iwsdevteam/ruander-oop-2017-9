<?php

/* 
 * Számlázási lakcím
 */

class AddressBusiness extends Address {
   
    //method override
    public function display(){
        $output = '<div class="residence alert alert-warning">';
        $output .= parent::display();
        $output .= '</div>';
        return $output;
    }
    
    protected function _init(){
        $this->_setAddressTypeId(parent::ADDRESS_TYPE_BUSINESS);
        return;
    }
}