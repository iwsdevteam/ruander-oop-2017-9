<?php

/* 
 * Ideiglenes cím
 */

class AddressTemporary extends Address {
   
    //method override
    public function display(){
        $output = '<div class="residence alert alert-info">';
        $output .= parent::display();
        $output .= '</div>';
        return $output;
    }
    
    protected function _init(){
        $this->_setAddressTypeId(parent::ADDRESS_TYPE_TEMPORARY);
        return;
    }
}