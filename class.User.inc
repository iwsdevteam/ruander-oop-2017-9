<?php

/* 
 *Felhasználó kezelő osztály
 */

class User implements Model{
    //felh. azonosító
    protected $_id;
    
    //felhasználó teljes név
    public $name;
    
    //felh. email
    public $email;
    //felh. jelszó
    private $_password;
    //felh. állapota
    protected $_status;
    //timestamps
    protected $_time_created;
    protected $_time_updated;
    
    /**
     * konstruktor
     * @param array $data
     */
    public function __construct($data=[]) {
        $this->_time_created = time();
        
    }
    
    /**
     * Felhasználó kiírása
     * @return string
     */
    public function  display(){
        return var_export($this,true);
    }
    /**
     * Felh. törlése
     * @return void
     */
    public function delete() {
        return null;
    }
    /**
     * Felhasználó mentése
     * @return saved object with given id
     */
    public function save() {
        
    }
    /**
     * Felhasználó módosítása
     * @return saved object with modified data  
     */
    public function update() {
        
    }
    /**
     * Felhasználó betöltése (find)
     * @param integer $id
     * @return mixed (object, false)
     */
    public static function load($id) {
        
    }

}
