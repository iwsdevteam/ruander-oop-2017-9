<?php 
function __autoload($classname) {
    $name = 'class.' . $classname . '.inc';
    include $name;
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <title>Ruander OOP PHP haladó tanfolyam</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 
        <!-- Saját stílusok-->
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
            <header class="row">
                <div class="col-12">
                    <h1>Ruander OOP PHP haladó tanfolyam</h1>
                </div>  
            </header>
             <div class="row">
                <section class="col-12 content">
                    <?php require "crud.php" ?>
                </section>  
            </div>
            <div class="row">
                <section class="col-12 content">
                    <?php require "test.php" ?>
                </section>  
            </div>
            <footer class="row">
                <div class="col-12 text-center">
                    Ruander &copy; <?php echo date('Y'); ?>
                </div>
            </footer>
        </div>


        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="js/jquery-slim.min.js" type="text/javascript"></script>
        <script src="js/popper.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>