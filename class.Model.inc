<?php

/** 
 *  Interface for models
 *  minden public és abstract
 */

interface Model{
    /**
     * Get all models
     * @return 'collection'
     */
    public static function all();
     /**
     * model betöltése (find)
     * @param integer $id
     * @return 
     */
    public static function load($id);
    /**
     * model mentése
     * @return saved object with given id
     */
    public function save();
    /**
     * model módosítása
     * @return saved object with modified data  
     */
    public function update();
    /**
     * model törlése
     * @return void
     */
    public function delete();
    
}