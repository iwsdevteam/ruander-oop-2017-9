<?php
/**
 * Adatbázis elérés kezelése only one connection allowed
 * Singleton - Egyke
 */

class Database{
    //kapcsolat
    private $_connection;
    //példány
    private static $_instance;
    
    /**
     * példány 'kiadása' ha volt az ha nem volt lesz :)
     * @return object (Database)
     */
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * Kapcsolat felépítése
     */
    public function __construct() {
        $this->_connection = new mysqli('localhost','root','','hgy_oop');
        //hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Nem sikerült csatlakozni az adatbázishoz : '.mysqli_connect_error(),E_USER_ERROR);
        }
    }
    //védelem klónozás ellen
    final public function __clone(){}
  
    //kacsolat kiadása
    public function getConnection(){
        return $this->_connection;
    }
}