<?php

//osztályok betöltése
//function __autoload($classname) {
//    $name = 'class.' . $classname . '.inc';
//    include $name;
//}

echo '<h2>Objektum származtatása</h2>';
echo '<h3>Állandó cím megadása - residence</h3>';
$data = [
    'street_address_1' => "Frangepán u. 3.",
    'city_name' => "Budaörs",
//    'country_name' => "Magyarország",
//    'address_type_id' => 1,
];
$address = new AddressResidence($data);
echo '<pre>' . var_export($address, true) . '</pre>';

echo $address;


//$postalCodeFinderTest = Address::postal_code_find('Göd');
//foreach ($postalCodeFinderTest as $result) {
//    echo '<pre>'.var_export($result,true).'</pre>';
//}
echo '<h3>Számlázási cím megadása - business</h3><pre>';
$data = [
    'street_address_1' => 'teszt utca 1.',
    'city_name' => 'Szentendre',
    'country_name' => 'Magyarország',
//    'address_type_id' => 2,
];
$address_2 = new AddressBusiness($data);
//$address_2->save();
var_dump($address_2);
echo $address_2;
//var_dump($address_2);

//echo '<h3>Osztály információk kinyerése objektumból</h3><pre>';
//$test = new ReflectionClass($address_2);
//var_dump(
//        $test->getProperties(),
//        $test->getParentClass(),
//        $test->getMethods()
//        );

//$user = new User;
//echo $user->display();
$test_array = ['alma','körte'];
//elem hozzáadása 1.
$test_array[]='barack';
var_dump($test_array);
