<?php

/* 
 * Saját kivételkezelés
 */

class ExceptionAddress extends Exception{
    
    public function __toString() {
        return __CLASS__.": {$this->message} : error code: [{$this->code}]";
    }
}