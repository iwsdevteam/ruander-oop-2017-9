<?php

/**
 * Cím kezelő osztály, irányítószám - város ellenőrzésre, keresésre
 * @todo Model interface alkalmazása
 */
abstract class Address implements Model {

    //osztály állandók (konstansok) -> elérhető az objektum létrehozása nélkül - osztályszintű
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;
    
    const ADDRESS_ERROR_NOT_FOUND = 1000;

    //megadható címtipusok - static -> elérhető az objektum létrehozása nélkül - osztályszintű
    public static $valid_address_types = [
        Address::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        self::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];
    //címsor 1
    public $street_address_1;
    //címsor 2
    public $street_address_2;
    //város név
    public $city_name;
    //városrész
    public $subdivision_name;
    //ország név
    public $country_name;
    //irányítószám
    protected $_postal_code;
    //cím tipus
    protected $_address_type_id;
    //cím azonosítója
    protected $_address_id;
    //létrehozás dátuma
    protected $_time_created = 123456;
    //módosítás dátuma
    protected $_time_updated;

    abstract protected function _init();

    /**
     * Magic __construct - new Classname
     * @param array $data
     * return void
     */
    public function __construct($data = []) {
//        var_dump(count($data));
        $this->_init();
        $this->_time_created = time();

        // tömb kell
        if (!is_array($data)) {
            trigger_error('Nem lehet objektumot létrehozni a kapott adatokból a(z)[' . get_class($name) . '] osztályból!');
        } else {
            foreach ($data as $name => $value) {//objektum felépítése a kapott tömb bejárásával
                if (in_array($name, [
                            'address_id', 'time_created', 'time_updated'
                        ])) {
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        }
    }

    /**
     * Magic __get
     * @param string $name
     * @return mixed
     */
    public function __get($name) {
        //ha nincs irányítószám, akkor állítsuk be
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }

        $protected_property_name = '_' . $name; //védett tul. név
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }

        //minden más esetben hiba trigger
        trigger_error("Nem létező vagy védett tulajdonságot próbálunk elérni a _get által. [$name]");
    }

    /**
     * Magic __set
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function __set($name, $value) {
        //kivétel address_type_id ellenőrzésre
        if ($name == 'address_type_id') {
            $this->_setAddressTypeId($value);
            return;
        }
        //kivétel irányítószámra
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }
        //minden más esetben hiba trigger
        trigger_error("Nem létező vagy védett tulajdonságot próbálunk megadni a _set által. [$name]");
    }

    /**
     * Magic __toString - echo (object)
     * @return string
     */
    public function __toString() {
        return $this->display();
    }

    /**
     * Irányítószám lekérése az adatbázisból, város és városrész alapján
     * @return string 
     */
    protected function _postal_code_search() {
        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        $qry = 'SELECT irsz ';
        $qry .= 'FROM telepulesek ';

        $city_name = $mysqli->real_escape_string($this->city_name);
        $qry .= 'WHERE telepules = "' . $city_name . '" ';

        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $qry .= 'AND telepulesresz = "' . $subdivision_name . '" ';

        //lekérés
        $result = $mysqli->query($qry);

        /**
         * @todo több eredmény kezelése és szabadszavas keresés
         */
        if ($row = $result->fetch_object()) {//ha találtunk, visszatérünk vele
            return $row->irsz;
        }
//        die(var_export($postal_code, true));
//        die('lekérve');
        return "Nem találtam";
    }

    /**
     * Staikus irányítószám kereső városnév részlet alapján 
     * @param string $part
     * @return string|JSon encoded
     */
    public static function postal_code_find($part) {
        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        $qry = 'SELECT * ';
        $qry .= 'FROM telepulesek ';

        $city_name = $mysqli->real_escape_string($part);
        $qry .= 'WHERE telepules LIKE "%' . $city_name . '%" ';

        $result = $mysqli->query($qry) or die('GEBASZ');
        $array = []; //ebbe pakoljuk
        while ($row = $result->fetch_object()) {
            $array[] = $row;
        }
        return $array;
    }

    /**
     * Cím html kiírása
     * @return string
     */
    public function display() {
        $output = $this->street_address_1; //címsor 1 
        if ($this->street_address_2) {
            $output .= '<br>' . $this->street_address_2; //címsor 2 ha nem üres
        }
        $output .= "<br>{$this->city_name}, $this->country_name";
        $output .= '<br>' . $this->postal_code;
        return $output;
    }

    /**
     * Címtipus azonosító ellenőrzése
     * @param int $address_type_id
     * @return boolean 
     */
    public static function isValidAddressTypeId($address_type_id) {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Védett eljárás érvényes cimntípus azonosító megadására
     * @param int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id) {
        //ha érvényes, állitsuk be
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
    }

    /**
     * Cím példányosítása
     */
    public static function getInstance($address_type_id, $data) {
        $class_name = 'Address' . self::$valid_address_types[$address_type_id];
        $ret = new $class_name($data);

        return $ret;
    }

    /**
     * Összes cím betöltése
     * @return array collection
     */
    public static function all() {
        $collection = []; //üres alap kollekció.
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $qry = "SELECT * FROM addresses";

        $result = $mysqli->query($qry);

        while ($row = $result->fetch_assoc()) {
            $collection[] = self::getInstance($row['address_type_id'], $row);
        }
        return $collection;
    }

    //egy model (cím) betöltése adatbázisból id alapján - final ->nem overrideolható
    final public static function load($id) {

        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $qry = 'SELECT * ';
        $qry .= 'FROM addresses ';
        $qry .= 'WHERE address_id = "' . (int) $id . '" ';

        $result = $mysqli->query($qry);
        if ($row = $result->fetch_assoc()) {
            return self::getInstance($row['address_type_id'], $row);
        }
        throw new ExceptionAddress("Can't load address with id [$id]",self::ADDRESS_ERROR_NOT_FOUND);
    }

    //egy model (cím) mentése adatbázisba
    public function save() {
        //db kapcsolat
        //adatbázis kapcsolat felépítése
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódlap illesztése
        $mysqli->set_charset("utf8");
        //query összeállítása
        //adatok alapján insert query összeállítása
        $qry = "INSERT INTO `addresses` (
            
            `street_address_1`,
            `street_address_2`,
            `postal_code`,
            `city_name`,
            `subdivision_name`,
            `address_type_id`,
            `country_name`,
            `time_created`,
            `time_updated`) 
            VALUES(
                '{$mysqli->real_escape_string($this->street_address_1)}',
                '{$mysqli->real_escape_string($this->street_address_2)}',
                '{$this->postal_code}',
                '{$mysqli->real_escape_string($this->city_name)}',
                '{$mysqli->real_escape_string($this->subdivision_name)}',
                {$this->address_type_id},
                '{$mysqli->real_escape_string($this->country_name)}',
                '" . date("Y-m-d H:i:s", $this->time_created) . "',
                '{$this->time_updated}')";
        //query futtatása vagy die
        $result = $mysqli->query($qry) or die('GEBASZ on Save');
        //a most kapott id megadása az objektumnak
        if ($mysqli->insert_id) {
            $this->_address_id = $mysqli->insert_id;
        }
        return;
    }

    //egy model (cím) módosítása az adatbázisban
    public function update() {
        
    }

    //egy model (cím) törlése adatbázisból
    public function delete() {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        
        $qry = "DELETE FROM addresses WHERE address_id = $this->_address_id LIMIT 1";
        $result = $mysqli->query($qry) or die('GEBASZ on Delete');
        return;
    }

}
